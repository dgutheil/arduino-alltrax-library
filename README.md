### Arduino Alltrax Library ###

An Arduino Library for interfacing an Arduino with an Alltrax AXE/DCX motor controller. Any information that the motor controller is monitoring can be viewed on the Arduino, and writeable parameters on the Alltrax can be written by the Arduino as well. It may work for other Alltrax series motor controllers, however only the AXE series has been tested.

### Set up ###

See here for a documentation:
http://derekkg2.github.io/arduino-alltrax-axe-library/